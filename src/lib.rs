mod yc;

pub type TriggersResult<T> = Result<T, Box<dyn std::error::Error>>;

pub mod prelude {
    pub use crate::yc::{queue::QueueTrigger, timer::TimerTrigger};
}
