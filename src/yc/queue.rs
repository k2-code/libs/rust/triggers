use crate::TriggersResult;
use serde::de::DeserializeOwned;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;

#[derive(Serialize, Deserialize, Debug)]
pub struct QueueTrigger<'a, B> {
    pub messages: Vec<Message<'a, B>>,
}

impl<'a, B> QueueTrigger<'a, B>
where
    B: Serialize + DeserializeOwned,
{
    pub fn to_json(&self) -> TriggersResult<String> {
        Ok(serde_json::to_string(self)?)
    }

    pub fn from_json(json: &str) -> TriggersResult<Self> {
        let json: &str = &json.replace('\\', "");
        let json: &str = &json.replace("\"{\"", "{\"");
        let json: &str = &json.replace("\"}\"", "\"}");

        Ok(serde_json::from_str(json)?)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Message<'a, B> {
    pub event_metadata: EventMetadata<'a>,
    pub details: Details<'a, B>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EventMetadata<'a> {
    pub event_id: Cow<'a, str>,
    pub event_type: Cow<'a, str>,
    pub created_at: Cow<'a, str>,
    pub cloud_id: Cow<'a, str>,
    pub folder_id: Cow<'a, str>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Details<'a, B> {
    pub queue_id: Cow<'a, str>,
    pub message: DetailsMessage<'a, B>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DetailsMessage<'a, B> {
    pub message_id: Cow<'a, str>,
    pub md5_of_body: Cow<'a, str>,
    pub body: B,
    pub attributes: Attributes<'a>,
    #[serde(skip_deserializing)]
    pub message_attributes: MessageAttributes<'a>,
    #[serde(skip_deserializing)]
    pub md5_of_message_attributes: Cow<'a, str>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all(deserialize = "PascalCase"))]
pub struct Attributes<'a> {
    pub approximate_first_receive_timestamp: Cow<'a, str>,
    pub approximate_receive_count: Cow<'a, str>,
    pub sent_timestamp: Cow<'a, str>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct MessageAttributes<'a> {
    pub message_attribute_key: MessageAttributeKey<'a>,
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all(deserialize = "camelCase"))]
pub struct MessageAttributeKey<'a> {
    pub data_type: Cow<'a, str>,
    pub string_value: Cow<'a, str>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::TriggersResult;
    use serde::{Deserialize, Serialize};

    const TRIGGER_JSON: &str = r#"{\"messages\":[{\"event_metadata\":{\"event_id\":\"ffb774f5-de3059f0-474a5bc4-6017eb45\",\"event_type\":\"yandex.cloud.events.messagequeue.QueueMessage\",\"created_at\":\"2022-12-02T16:28:57.090Z\",\"tracing_context\":null,\"cloud_id\":\"b1go2vr0c15o27ikgt98\",\"folder_id\":\"b1gsrlmq528h5fq8cpb0\"},\"details\":{\"queue_id\":\"yrn:yc:ymq:ru-central1:b1gsrlmq528h5fq8cpb0:passport-login\",\"message\":{\"message_id\":\"ffb774f5-de3059f0-474a5bc4-6017eb45\",\"md5_of_body\":\"0a0724f8813b760a31815cb48113c748\",\"body\":\"{\\\"event\\\":\\\"passport_login\\\",\\\"phone\\\":\\\"+79633333333\\\",\\\"message\\\":\\\"This is your confirmation code: 57525. Don't tell it anyone!\\\"}\",\"attributes\":{\"ApproximateFirstReceiveTimestamp\":\"1669998537254\",\"ApproximateReceiveCount\":\"1\",\"SentTimestamp\":\"1669998537090\"},\"message_attributes\":{},\"md5_of_message_attributes\":\"\"}}}]}"#;

    #[derive(Serialize, Deserialize, Debug)]
    struct Body {
        event: String,
        phone: String,
        message: String,
    }

    #[test]
    fn can_queue_trigger_from_json() -> TriggersResult<()> {
        QueueTrigger::<Body>::from_json(TRIGGER_JSON)?;
        Ok(())
    }

    #[test]
    fn can_queue_trigger_to_json() -> TriggersResult<()> {
        let queue: QueueTrigger<Body> = QueueTrigger::<Body>::from_json(TRIGGER_JSON)?;

        queue.to_json()?;
        Ok(())
    }

    #[test]
    fn can_event_metadata_from_json() -> TriggersResult<()> {
        let json: &str = r#"
        {
            "event_id": "test queue id",
            "event_type": "test event type",
            "created_at": "test created at",
            "cloud_id": "test cloud id",
            "folder_id": "test folder id"
        }"#;

        serde_json::from_str::<EventMetadata>(json)?;
        Ok(())
    }

    #[test]
    fn can_details_from_json() -> TriggersResult<()> {
        let json: &str = r#"
        {
            "queue_id": "test queue id",
            "message":{
               "message_id": "test message id",
               "md5_of_body": "test md5 of body",
               "body": {"event":"passport_login","phone":"+14152370801","message":"This is your confirmation code: 97262. Don't tell it anyone!"},
               "attributes": {
                    "ApproximateFirstReceiveTimestamp": "test ApproximateFirstReceiveTimestamp",
                    "ApproximateReceiveCount": "test ApproximateReceiveCount",
                    "SentTimestamp": "test sent timestamp"
               }
            }
        }"#;

        serde_json::from_str::<Details<Body>>(json)?;
        Ok(())
    }

    #[test]
    fn can_details_message_from_json() -> TriggersResult<()> {
        let json: &str = r#"
        {
           "message_id": "test message id",
           "md5_of_body": "test md5 of body",
           "body": {"event":"passport_login","phone":"+14152370801","message":"This is your confirmation code: 97262. Don't tell it anyone!"},
           "attributes": {
                "ApproximateFirstReceiveTimestamp": "test ApproximateFirstReceiveTimestamp",
                "ApproximateReceiveCount": "test ApproximateReceiveCount",
                "SentTimestamp": "test sent timestamp"
           }
        }"#;

        serde_json::from_str::<DetailsMessage<Body>>(json)?;
        Ok(())
    }

    #[test]
    fn can_attributes_from_json() -> TriggersResult<()> {
        let json: &str = r#"
        {
            "ApproximateFirstReceiveTimestamp": "test ApproximateFirstReceiveTimestamp",
            "ApproximateReceiveCount": "test ApproximateReceiveCount",
            "SentTimestamp": "test sent timestamp"
        }"#;

        serde_json::from_str::<Attributes>(json)?;
        Ok(())
    }

    #[test]
    fn can_message_attributes_from_json() -> TriggersResult<()> {
        let json: &str = r#"
        {
            "messageAttributeKey": {
                "dataType": "test data type",
                "stringValue": "test string value"
            }
        }"#;

        serde_json::from_str::<MessageAttributes>(json)?;
        Ok(())
    }

    #[test]
    fn can_message_attribute_key_from_json() -> TriggersResult<()> {
        let json: &str = r#"
        {
            "dataType": "test data type",
            "stringValue": "test string value"
        }"#;

        serde_json::from_str::<MessageAttributeKey>(json)?;
        Ok(())
    }
}
