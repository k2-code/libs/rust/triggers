use crate::TriggersResult;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;

#[derive(Serialize, Deserialize, Debug)]
pub struct TimerTrigger<'a> {
    pub event_metadata: EventMetadata<'a>,
    pub details: Details<'a>,
}

impl<'a> TimerTrigger<'a> {
    pub fn to_json(&self) -> TriggersResult<String> {
        Ok(serde_json::to_string(self)?)
    }

    pub fn from_json(json: &str) -> TriggersResult<Self> {
        Ok(serde_json::from_str(json)?)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct EventMetadata<'a> {
    pub event_id: Cow<'a, str>,
    pub event_type: Cow<'a, str>,
    pub created_at: Cow<'a, str>,
    pub cloud_id: Cow<'a, str>,
    pub folder_id: Cow<'a, str>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Details<'a> {
    pub trigger_id: Cow<'a, str>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::TriggersResult;
    use serde::{Deserialize, Serialize};

    #[derive(Serialize, Deserialize)]
    struct Body {
        message: String,
    }

    #[test]
    fn can_timer_from_json() -> TriggersResult<()> {
        let json: &str = r#"
        {
            "event_metadata": {
                "event_id": "a1s41g2n5g0o5p778k4r",
                "event_type": "yandex.cloud.events.serverless.triggers.TimerMessage",
                "created_at": "2019-12-04T12:05:14.227761Z",
                "cloud_id": "b1gvlrnlei4l5idm9cbj",
                "folder_id": "b1g88tflru0ek1omtsu0"
            },
            "details": {
                "trigger_id": "a1sfe084v4se4morbu2i"
            }
        }"#;

        TimerTrigger::from_json(json)?;
        Ok(())
    }

    #[test]
    fn can_timer_to_json() -> TriggersResult<()> {
        let json: &str = r#"
        {
            "event_metadata": {
                "event_id": "a1s41g2n5g0o5p778k4r",
                "event_type": "yandex.cloud.events.serverless.triggers.TimerMessage",
                "created_at": "2019-12-04T12:05:14.227761Z",
                "cloud_id": "b1gvlrnlei4l5idm9cbj",
                "folder_id": "b1g88tflru0ek1omtsu0"
            },
            "details": {
                "trigger_id": "a1sfe084v4se4morbu2i"
            }
        }"#;

        let queue: TimerTrigger = TimerTrigger::from_json(json)?;

        queue.to_json()?;
        Ok(())
    }

    #[test]
    fn can_event_metadata_from_json() -> TriggersResult<()> {
        let json: &str = r#"
        {
            "event_id": "a1s41g2n5g0o5p778k4r",
            "event_type": "yandex.cloud.events.serverless.triggers.TimerMessage",
            "created_at": "2019-12-04T12:05:14.227761Z",
            "cloud_id": "b1gvlrnlei4l5idm9cbj",
            "folder_id": "b1g88tflru0ek1omtsu0"
        }"#;

        serde_json::from_str::<EventMetadata>(json)?;
        Ok(())
    }

    #[test]
    fn can_details_from_json() -> TriggersResult<()> {
        let json: &str = r#"
        {
            "trigger_id": "a1sfe084v4se4morbu2i"
        }"#;

        serde_json::from_str::<Details>(json)?;
        Ok(())
    }
}
